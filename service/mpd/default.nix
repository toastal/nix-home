{ config, lib, pkgs ? import <nixpkgs>, ... }:

with lib;

{
  services.mpd = {
    enable = true;

    musicDirectory = "/music";
  };
}
