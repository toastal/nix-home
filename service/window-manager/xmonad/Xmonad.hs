{-# LANGUAGE UnicodeSyntax #-}

import           Data.Monoid                  (Endo, (<>))
import           GHC.IO.Handle.Types          (Handle)

import           XMonad
import           XMonad.Actions.UpdatePointer (updatePointer)
import           XMonad.Config.Desktop        (desktopConfig)
import           XMonad.Hooks.DynamicLog      (PP, dynamicLogWithPP, ppCurrent,
                                               ppOutput, ppSort, ppTitle,
                                               shorten, statusBar, wrap,
                                               xmobarColor, xmobarPP)
import           XMonad.Hooks.EwmhDesktops    (fullscreenEventHook)
import           XMonad.Hooks.FadeInactive    (fadeIf, fadeOutLogHook)
import           XMonad.Hooks.ManageDocks     (AvoidStruts, avoidStruts)
import           XMonad.Hooks.ManageHelpers   (doFullFloat, isFullscreen)
import           XMonad.Layout.LayoutModifier (ModifiedLayout)
import           XMonad.Layout.NoBorders      (SmartBorder, smartBorders)
import           XMonad.Layout.ThreeColumns   (ThreeCol (..))
import           XMonad.Util.EZConfig         (additionalKeysP)
import           XMonad.Util.NamedScratchpad  (namedScratchpadFilterOutWorkspace)
import           XMonad.Util.Run              (spawnPipe)
import           XMonad.Util.WorkspaceCompare (getSortByTag)


main ∷ IO ()
main = do
  xmobarConf ← statusBar "xmobar" xmobarPP' toggleStrutsKey xmonadConf
  xmonad xmobarConf


xmonadConf ∷ XConfig (ModifiedLayout SmartBorder (ModifiedLayout AvoidStruts (Choose Tall (Choose (Mirror Tall) (Choose ThreeCol Full)))))
xmonadConf =
  desktopConfig
  { borderWidth = 1
  , normalBorderColor = "#3e4074"
  , focusedBorderColor = "#bfcdcc"
  , handleEventHook = fullscreenEventHook -- fixes fullscreen bug in Chromium
  , layoutHook = smartBorders . avoidStruts $ layoutHook'
  , manageHook = manageHook'
  , modMask = mod4Mask -- Super as mod
  , terminal = "kitty"
  , workspaces = map show $ [1..9] -- workspaces'
  } `additionalKeysP`
  keys'

keys' ∷ [(String, X ())]
keys' =
  [ ("M-o", spawn "rofi -show run")
  , ("M-u", spawn "xkblayout-state set +1")
  , ("M-i", spawn "xkblayout-state set -1")
  ]

xmobarPP' ∷ PP
xmobarPP' =
  xmobarPP
  { ppCurrent = xmobarColor "#ff8787" "" . wrap "[" "]" -- #9BC1B2
  , ppTitle = xmobarColor "#00d7ff" "" . shorten 58 -- #9BC1B2
  , ppSort = fmap (. namedScratchpadFilterOutWorkspace) getSortByTag
  }

toggleStrutsKey ∷ XConfig Layout → ( KeyMask, KeySym )
toggleStrutsKey XConfig { XMonad.modMask = modMask } =
  ( modMask, xK_b )


layoutHook' ∷ Choose Tall (Choose (Mirror Tall) (Choose ThreeCol Full)) a
layoutHook' =
  tall ||| Mirror tall ||| three ||| Full
  where
    tall = Tall 1 (1/100) (1/2)
    three = ThreeCol 1 (3/100) (1/2)

-- Window management
manageHook' ∷ Query (Endo WindowSet)
manageHook' =
  composeAll
    [ className =? "MPlayer" --> doFloat
    , className =? "mpv" --> doFloat
    , className =? "zoom" --> doFloat
    , className =? "peek" --> doFloat
    , className =? "stalonetray" --> doIgnore
    , isFullscreen --> doFullFloat
    ]

--workspaces' ∷ [String]
--workspaces' =
--  map xdo
--    [ ( 1, "๑" )
--    , ( 2, "๒" )
--    , ( 3, "๓" )
--    , ( 4, "๔" )
--    , ( 5, "๕" )
--    , ( 6, "๖" )
--    , ( 7, "๗" )
--    , ( 8, "๘" )
--    , ( 9, "๙" )
--    ]
--  where
--    xdo ( n, s ) =
--      " show n <> "`>" <> s <> ""
