Config
  { font = "xft:Terminus:size=16:regular:antialias=false"
  , additionalFonts = []
  , border = NoBorder
  , borderColor = "#131217"
  , bgColor = "#131217"
  , fgColor = "grey"
  , alpha = 255
  , position = Top
  , textOffset = -1
  , iconOffset = -1
  , lowerOnStart = True
  , pickBroadest = False
  , persistent = False
  , hideOnStart = False
  , iconRoot = "."
  , allDesktops = True
  , overrideRedirect = True
  , commands =
    --Run Brightness
      --[ "--template", "<bar> <percent>%"
      --, "-D", "intel_backlight"
      --, "-C", "actual_brightness"
      --, "-M", "max_brightness"
      --] 100
    --Run MPD
      --[ "--template", "<composer> <title> (<album>) <track>/<plength> <statei> [<flags>]"
      -- , "--", "-P", ">>", "-Z", "|", "-S", "><"
      --] 10
    --[ Run Com "cryptocurrencies-prices"
    --  [ "--crypto", "BTC"
    --  , "--currency", "USD"
    --  ] "btc" 2400
    --, Run Com "cryptocurrencies-prices"
    --  [ "--crypto", "ETH"
    --  , "--currency", "USD"
    --  ] "eth" 1800
    --, Run Com "cryptocurrencies-prices"
    --  [ "--crypto", "LTC"
    --  , "--currency", "USD"
    --  ] "ltc" 2800
    --, Run Com "currency-usd"
    --  []
    --  "currency" 60000
    [ Run Weather "VTBD"
      [ "--template", "<tempC>°C, <skyCondition>"
      , "--Low", "40"
      , "--High", "85"
      , "--normal","#5fd7d7"
      , "--high","#ff8787"
      , "--low","lightblue"
      ] 10000
    , Run CoreTemp
      [ "--template", "CPU <core0>°C"
      , "--Low", "70"
      , "--High", "80"
      , "--low", "#5fd7d7"
      , "--normal", "darkorange"
      , "--high", "#ff8787"
      ] 600
    , Run BatteryP [ "BAT0" ]
      [ "--template", "BAT <acstatus>"
      , "--Low", "10"
      , "--High", "80"
      , "--low", "#ff8787"
      , "--normal", "darkorange"
      , "--high", "#5fd7d7"
      , "--" -- battery specific options
        -- discharging status
      , "-o", "<left>% (<timeleft>)"
        -- AC "on" status
      , "-O", "<fn=1></fn> <left>%"
        -- charged status
      , "-i", "<left>%"
      ] 200
    , Run Wireless "wlo1"
      [ "--template", "<essid>" -- <qualityvbar>"
      ] 200
    , Run Kbd
      [ ( "us(dvorak)", "DV" )
      , ( "us", "US" )
      , ( "th", "TH" )
      ]
    , Run Date "%a %b %_d %Y %H:%M" "date" 10
    , Run UnsafeStdinReader
    ]
  , sepChar = "%"
  , alignSep = "}{"
  , template =
    "%UnsafeStdinReader%}\
\{%wlp59s0wi% | %kbd% | %VTBD% | %coretemp% | %battery% | <fc=#afafd7>%date%</fc>"
  }
