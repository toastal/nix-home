{ config, lib, pkgs ? import <nixpkgs>, ... }:

with lib;

{
  home.packages = with pkgs; [
    kitty
    nitrogen
    rofi
    scrot
    #standalonetray
    xorg.xinit
  ];

  xsession.windowManager.xmonad = {
    enable = true;
    enableContribAndExtras = true;
    extraPackages = haskellPackages: [
      haskellPackages.xmonad-contrib
    ];
    config = pkgs.writeText "xmonad.hs" "${builtins.readFile ./Xmonad.hs}";
  };

  programs.xmobar = {
    enable = true;
    extraConfig = "${builtins.readFile ./Xmobar.hs}";
  };
}
