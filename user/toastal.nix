{ config, lib, pkgs ? <nixpkgs> { }, ... }:

with lib;

{
  imports = [
    #../data/font/iosevka/fixed.nix
    ../program/editor/neovim/default.nix
    ../program/multiplexer/tmux/default.nix
    ../program/networking/mail-calendar-contacts.nix
    #../program/networking/instant-messenger/matterhorn/default.nix
    ../program/networking/irc/senpai/default.nix
    ../program/security/pass/default.nix
    ../program/shell/fish/default.nix
    #../program/terminal/kitty/default.nix
  ];


  home.packages = with pkgs; [
    any-nix-shell
    cachix
    niv
    nix-prefetch-scripts
    nixpkgs-fmt
    nox

    # system tools
    exa
    fzf
    gotop
    mosh
    ripgrep
    tree
    tree-sitter
    xdg-utils

    # development tools
    dhall
    dhall-json
    gitAndTools.git-review
    gitAndTools.git-trim
    git-branchless
    jq
    lazygit

    # networking
    himalaya
    isync
    khal
    qrencode
    vdirsyncer

    # fonts
    # chonburi-font
    # iosevka-fixed
    # julia-mono
    # kanit-font

    # other
    lesspass-cli
    librsvg
    matterhorn
    minisign
    neofetch
    onlykey-cli
    senpai
    #(weechat.override {
    #  configure = { availablePlugins, ... }: {
    #    scripts = with pkgs.weechatScripts; [
    #      weechat-matrix
    #      # weechat-xmpp
    #    ];
    #  };
    #})
    youtube-dl
    zbar
  ];

  programs.git = {
    enable = true;
    package = pkgs.gitAndTools.gitFull;
    userEmail = "toastal@posteo.net";
    userName = "toastal";
    signing = {
      key = "5CCE6F1466D47C9E";
    };
    ignores = [
      "/.direnv/"
      "/result"
      ".DS_Store"
      ".DS_Store?"
      "._*"
      ".Trashes"
      "ethumbs.db"
      "Thumbs.db"
      ":wq?"
      "*.sw[opq]"
    ];
    includes = [
      {
        condition = "gitdir:~/Platonic/";
        path = "~/Platonic/.gitconfig";
      }
    ];
    extraConfig = {
      core = {
        editor = "nvim";
        diff = "nvim -d";
      };
      init = {
        defaultBranch = "trunk";
      };
      sendmail = {
        smtpencryption = "tls";
        smtpserver = "posteo.de";
        smtpuser = "toastal@posteo.net";
        smtpserverport = 587;
      };
      rerere = {
        enabled = "true";
      };
    };
  };

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };

  home.language = {
    base = "en_US.UTF-8";
    ctype = "en_US.UTF-8";
    numeric = "en_US.UTF-8";
    time = "en_CA.UTF-8";
    collate = "en_US.UTF-8";
    messages = "en_US.UTF-8";
    paper = "en_GB.UTF-8";
  };

  home.sessionVariables = {
    LANGUAGE = "en_US:en";
    TERMINAL = "kitty";
    EDITOR = "nvim";
    VISUAL = "nvim";
    BROWSER = "firefox";
    SLIMERJSLAUNCHER = "firefox";
    BAT_THEME = "base16";
    DO_NOT_TRACK = "1";
    MINISIGN_CONFIG_DIR = "\${XDG_DATA_HOME}/minisign";
  };
}
