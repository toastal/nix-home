let
  region_code = "TH";
  default_locale = "en-US";

in {
  "browser.search.region" = region_code;
  "browser.search.isUS" = true;
  "browser.ssb.enabled" = true;
  "browser.uidensity" = 1;
  "devtools.toolbox.tabsOrder" = "inspector,webconsole,netmonitor,jsdebugger,styleeditor,performance,memory,storage,accessibility";
  "distribution.searchplugins.defaultLocale" = default_locale;
  "general.useragent.locale" = default_locale;
  "extensions.pocket.enabled" = false;
  "extensions.ui.locale.hidden" = false;
  "gfx.color_management.enablev4" = true;
  "intl.accept_langauges" = "en-US,en,th";
  "layout.css.visited_links_enabled" = false;
  "network.prefetch-next" = "false";
  "privacy.trackingprotection.enabled" = true;
  "privacy.trackingprotection.cryptomining.enabled" = true;
  "privacy.trackingprotection.fingerprinting.enabled" = true;
  "privacy.trackingprotection.socialtracking.enabled" = true;
  "privacy.trackingprotection.pbmode.enabled" = true;
}
