let
  default_locale = "en-US";

in {
  "browser.startup.page" = 3;
  "browser.uidensity" = 1;
  "devtools.theme" = "dark";
  "devtool.toolbox.tabsOrder" = "inspector,webconsole.netmonitor,jsdebugger,styleeditor,storage,performance,memory,accessibility";
  "distribution.searchplugins.defaultLocale" = default_locale;
  "extensions.pocket.enabled" = false;
  "general.warnOnAboutConfig" = false;
  "general.useragent.locale" = default_locale;
  "gfx.color_management.enablev4" = true;
  "intl.accept_languages" = "en-US,en,th";
  "layout.css.visited_links_enabled" = false;
  "mousebutton.4th.enable" = false;
  "mousebutton.5th.enable" = false;
  "network.cookie.cookieBehavior" = 1;
  "network.dns.disablePrefetch" = true;
  "network.prefetch-next" = true;
  "privacy.socialtracking.block_cookies.enable" = true;
  "privacy.trackingprotection.cryptomining.enabled" = true;
  "privacy.trackingprotection.enabled" = true;
  "privacy.trackingprotection.fingerprinting.enabled" = true;
  "privacy.trackingprotection.pbmode.enabled" = true;
  "privacy.trackingprotection.socialtracking.enabled" = true;
}
