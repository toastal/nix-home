{ config, lib, pkgs, ... }:

with lib;

#let
#  moz_overlay = import (builtins.fetchTarball "https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz");
#  nixpkgs = import <nixpkgs> { config.allowUnfree = true; overlays = [ moz_overlay ]; };
#in {
{
  programs.firefox = {
    enable = true;
    #package = nixpkgs.latest.firefox-beta-bin;
    #package = pkgs.firefox-wayland;
    #package = pkgs.firefox-beta-bin;
    package = pkgs.wrapFirefox pkgs.firefox-unwrapped {
      forceWayland = true;
      extraPolicies = {
        Cookies = {
          AcceptThirdParty = "never";
          RejectTracker = true;
          Locked = false;
        };
      };
    };
  };
}
