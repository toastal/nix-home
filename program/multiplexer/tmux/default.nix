{ config, lib, pkgs ? import <nixpkgs>, ... }:

with lib;

{
  programs.tmux = {
    enable = true;
    clock24 = true;
    keyMode = "vi";
    terminal = "screen-256color";
    extraConfig = ''
      set -g mouse on
      set -g mouse-select-pane on
    '';
  };
}
