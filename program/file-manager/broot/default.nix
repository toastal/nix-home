{ config, lib, pkgs ? import <nixpkgs>, ... }:

with lib;

{
  programs.broot = {
    enable = true;
    enableFishIntegration = true;
  };
}
