{ config, lib, pkgs, ... }:

with lib;

let
  my_plugins = import ./plugins.nix { inherit pkgs; inherit (pkgs) vimUtils fetchFromGitHub fetchFromSourcehut; };

in
{
  programs.neovim = {
    enable = true;

    viAlias = false;
    vimAlias = true;
    vimdiffAlias = true;

    withPython3 = true;
    extraPython3Packages = ps: with ps; [
      pynvim
      requests
      simple-websocket-server
    ];

    extraConfig = import ./init.vim.nix;

    plugins = with (pkgs.vimPlugins // my_plugins); [
      color-scheme-test
      vim-lapis256
      sugilite256-vim
      lualine-nvim
      lualine-lsp-progress
      rainbow_parentheses-vim

      vim-rooter
      vim-devicons
      vim-surround
      tabular
      vim-fugitive
      comment-nvim

      # ale
      neoformat
      editorconfig-vim
      telescope-nvim
      vim-gitgutter
      nvim-lspconfig
      completion-nvim
      diagnostic-nvim

      vim-nix
      dhall-vim
      psc-ide-vim
      vim-graphql
      purescript-vim
      haskell-vim
      vim-stylish-haskell
      elm-vim
      vim-sugarss
      vim-pug
      # rails
      vim-fish
      # vint
      vader-vim
      # vim-coffee-script
      vim-toml
      json5-vim
      po-vim
      vim-diagram

      himalaya-vim
      goyo-vim
      limelight-vim
      vim-pencil
      nvim-colorizer-lua
      #nvim-ghost
    ];

    extraPackages = with pkgs; [
      fzf
      ripgrep
    ];
  };
}
