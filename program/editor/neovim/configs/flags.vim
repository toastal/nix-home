let g:IS_256_COLOR_TERM = $TERM ==? 'xterm-256color' || $TERM ==? 'xterm-termite' || $TERM ==? 'xterm-kitty' || $TERM ==? 'screen-256color'
let g:IS_TRUECOLOR_TERM = $COLORTERM ==? 'truecolor' || '$COLORTERM' ==? '24bit'

" let g:VPN_STATUS = system("command -v nordvpn > /dev/null && nordvpn status | awk -F ': ' '{ if (match($1, \"Status\")) { print $2 } }'")
