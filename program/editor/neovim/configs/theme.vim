if g:IS_256_COLOR_TERM
  if g:IS_TRUECOLOR_TERM && has('termguicolors')
    set termguicolors
  else
    set t_Co=256
  endif
  set background=dark
  "colorscheme lapis256
  colorscheme sugilite256
  " TODO get the background color from a config or something so Limelight
  " works
  "if $TERM ==? 'xterm-kitty'
  "  let b:term_bg = system("awk '{ if (match($1, \"^background\")) {print $2} }' $XDG_CONFIG_HOME/kitty/kitty.conf | head -n1")
  "endif
  "if !empty(b:term_bg)
  "  exe 'highlight Normal guifg=none ctermfg=none guibg=' . b:term_bg . ' ctermbg=none'
  "else
  "highlight Normal guifg=none ctermfg=none guibg=none ctermbg=none
  "endif
  " Rainbow Parens colors
  let g:rbpt_colorpairs = [
    \ [210, '#ff8787'],
    \ [075, '#5fafff'],
    \ [214, '#ffaf00'],
    \ [045, '#00d7ff'],
    \ [086, '#5fffd7'],
    \ [146, '#afafd7'],
    \ [051, '#00ffff'],
    \ [249, '#b2b2b2'],
    \ [080, '#5fd7d7'],
    \ ]
  if g:IS_TRUECOLOR_TERM
    " https://github.com/nanotee/nvim-lua-guide#a-note-about-packages
    "packadd! nvim-colorizer.lua
    " Colorizer
    lua require('colorizer').setup({
      \ '*';
      \ dhall = { rgb_fn = true; hsl_fn = true, RGBA = true, RRGGBBAA = true };
      \ css = { rgb_fn = true; hsl_fn = true, RGBA = true, RRGGBBAA = true };
      \ less = { rgb_fn = true; hsl_fn = true, RGBA = true, RRGGBBAA = true };
      \ sass = { rgb_fn = true; hsl_fn = true, RGBA = true, RRGGBBAA = true};
      \ sugarss = { rgb_fn = true; hsl_fn = true, RGBA = true, RRGGBBAA = true};
      \ },
      \ { names = false }
      \ )
  endif
endif

function! MyFiletype()
  return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft') : ''
endfunction

function! MyFileformat()
  return winwidth(0) > 70 ? (&fileformat . ' ' . WebDevIconsGetFileFormatSymbol()) : ''
endfunction

"au VimEnter * if g:colors_name =~ 'lapis256' | highlight Normal guifg=none ctermfg=none guibg=none ctermbg=none | endif

" Rainbow Parans
let g:bold_parentheses = 0
"augroup rainbowtime
"  au VimEnter * RainbowParenthesesToggle
"  au Syntax * RainbowParenthesesLoadRound
"  au Syntax * RainbowParenthesesLoadSquare
"  au Syntax * RainbowParenthesesLoadBraces
"augroup END
