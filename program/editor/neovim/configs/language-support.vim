" PureScript
let purescript_indent_case = 3
let purescript_indent_if = 3
let purescript_indent_let = 3
let purescript_indent_in = 3
let purescript_indent_where = 0
let purescript_indent_do = 3
let purescript_indent_dot = 0

" PSC IDE
let g:psc_ide_server_port = 4242

" Haskell
let g:haskell_indent_disable = 1
let g:haskell_indent_bare_where = 2
let g:haskell_indent_before_where = 0
let g:haskell_indent_case = 2
let g:haskell_indent_do = 2
let g:haskell_indent_guard = 2
let g:haskell_indent_if = 2
let g:haskell_indent_in = 2
let g:haskell_indent_where = 2

" Markdown
let g:markdown_fenced_languages =
  \ [ 'bash=sh'
  \ , 'css'
  \ , 'dhall'
  \ , 'elm'
  \ , 'hs=haskell'
  \ , 'haskell'
  \ , 'javascript'
  \ , 'js=javascript'
  \ , 'json=javascript'
  \ , 'nix'
  \ , 'purescript'
  \ , 'purs=purescript'
  \ , 'sass'
  \ , 'sh'
  \ , 'sss=sugarss'
  \ , 'sugarss'
  \ , 'xml'
  \ ]

" Neoformat
let g:neoformat_enable_dhall = [ 'dhall' ]
let g:neoformat_enable_elm = [ 'elmformat' ]
let g:neoformat_enable_nix = [ 'nixpkgsfmt' ]
let g:neoformat_enable_purescript = [ 'purstidy' ]
let g:neoformat_enable_ruby = [ 'rubocop' ]
let g:neoformat_enable_javascript = [ 'prettier' ]
let g:neoformat_enable_typescript = [ 'prettier' ]

augroup fmt
  autocmd!
  autocmd BufWritePre * undojoin | Neoformat
augroup END

" Neovim’s LSP
filetype plugin on
set updatetime=300
set completeopt=menuone
set completeopt+=noinsert
set completeopt-=preview
set shortmess+=c

function! LspStatus() abort
  if luaeval('#vim.lsp.buf_get_clients() > 0')
    return luaeval("require('lsp-status').status()")
  endif
  return ''
endfunction
