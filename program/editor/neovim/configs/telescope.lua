require('telescope').setup{
  defaults = {
    prompt_prefix = "» ",
    selection_caret = "» ",
    file_ignore_patterns = { "*.lock" },
    border = {},
    borderchars = { "━", "┃", "━", "┃", "┏", "┓", "┛", "┗" },
    use_less = true,
    path_display = {},
    set_env = { ['COLORTERM'] = 'truecolor' }, -- default = nil,
  }
}
