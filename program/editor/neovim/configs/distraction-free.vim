" Goyo
let g:goyo_width = 80
let g:goyo_height = '92%'
function! s:goyo_enter()
  Limelight 
  PencilSoft
endfunction
function! s:goyo_leave()
  Limelight!
  highlight Normal guifg=none ctermfg=none guibg=none ctermbg=none
endfunction
autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()


" Limelight
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240


" Pencil
let g:pencil#textwidth = g:goyo_width
augroup pencil
  autocmd!
  autocmd FileType text call pencil#init({'wrap': 'soft'})
augroup END
