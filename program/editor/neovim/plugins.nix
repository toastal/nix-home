{ pkgs, vimUtils, fetchFromGitHub, fetchFromSourcehut }:

# See existing versions at:
# https://github.com/NixOS/nixpkgs/blob/master/pkgs/misc/vim-plugins/generated.nix
{
  color-scheme-test = vimUtils.buildVimPluginFrom2Nix {
    pname = "color-scheme-test";
    version = "2003-05-14";
    src = fetchFromGitHub {
      owner = "vim-scripts";
      repo = "Color-Scheme-Test";
      rev = "51af4faa229ef2c30b06ad0065164569a74ed397";
      sha256 = "sha256-61lWKpLlMpRu2dnM4YC+6GqioAruQUqX+XhAUjtHVpM=";
    };
    nativeBuildInputs = with pkgs; [ dos2unix ];
    prePatch = "dos2unix **/*.vim";
  };

  json5-vim = vimUtils.buildVimPluginFrom2Nix {
    pname = "json5.vim";
    version = "2019-04-01";
    src = fetchFromGitHub {
      owner = "gutenye";
      repo = "json5.vim";
      rev = "1e1718959b33b9af9406d0ebf61cfcd71e274385";
      sha256 = "03m8ck9r39b14763ghl1ib52sb0f1hskyy4ajnjdhq58016z5w17";
    };
  };

  # vim-ghost??
  nvim-ghost = vimUtils.buildVimPluginFrom2Nix {
    pname = "nvim-ghost.nvim";
    version = "2021-07-11";
    src = fetchFromGitHub {
      owner = "subnut";
      repo = "nvim-ghost.nvim";
      rev = "052df0c4be7c57bc42ef94992adbfada69ba258c";
      sha256 = "sha256-IuGL2fMSA9VangKp74XQ0PEzu7ZIkd/yBvU16vZOoWs=";
    };
    passthru.python3Dependiencies = ps: with ps; [ pynvim requests simple-websocket-server ];
    dependencies = with pkgs; [ python3 ];
    configurePhase = ''
      substituteInPlace plugin/nvim_ghost.vim \
        --replace "if get(g:,'nvim_ghost_use_script', 0)" \
        "let g:nvim_ghost_use_script = 1 | let g:nvim_ghost_python_executable = '${pkgs.python3}/bin/python' | if v:true"
      substituteInPlace autoload/nvim_ghost.vim \
        --replace "if get(g:,'nvim_ghost_use_script', 0)" \
        "let g:nvim_ghost_use_script = 1 | let g:nvim_ghost_python_executable = '${pkgs.python3}/bin/python' | if v:true"
      rm -rf scripts/
    '';
  };

  po-vim = vimUtils.buildVimPluginFrom2Nix {
    pname = "po-vim";
    version = "2010-10-17";
    src = fetchFromGitHub {
      owner = "vim-scripts";
      repo = "po.vim";
      rev = "1a40565fd5b660b62a1dcc6a7ba45b76744a66fd";
      sha256 = "1x0p7rvncq2bb0ql51car6cffr9zvvdgyxnah1bc8fabayblwn5n";
    };
    dependencies = [ ];
  };

  sugilite256-vim = vimUtils.buildVimPluginFrom2Nix {
    pname = "sugilite256-vim";
    version = "2021-09-04T12.46";
    src = /home/toastal/Incantations/sugilite256;
    configurePhase = "cd vim/";
  };

  vim-diagram = vimUtils.buildVimPluginFrom2Nix {
    pname = "vim-diagram";
    version = "2020-07-02";
    src = fetchFromGitHub {
      owner = "zhaozg";
      repo = "vim-diagram";
      rev = "bd94f79ac6d701caf52a1b5547069c7b93cf6acf";
      sha256 = "sha256-CWhr0e2IxICb3WMYMFqRmPWJUiSlCWL7KRRxL0gzBkA=";
    };
    dependencies = [ ];
  };

  vim-lapis256 = vimUtils.buildVimPluginFrom2Nix {
    pname = "vim-lapis256";
    version = "2019-10-23";
    src = fetchFromGitHub {
      owner = "andrwb";
      repo = "vim-lapis256";
      rev = "7785e47162e1c28f6246b0f9a445df0399b2d3b3";
      sha256 = "0l3q2l1sxhlhyiwirffabv180j15z1lf3xqgivj04149m5xczb9p";
    };
    dependencies = [ ];
  };

  vim-sugarss = vimUtils.buildVimPluginFrom2Nix {
    pname = "vim-sugarss";
    version = "2016-08-12";
    src = fetchFromGitHub {
      owner = "hhsnopek";
      repo = "vim-sugarss";
      rev = "002c941d5d92f083e862097b51301ebb80849eed";
      sha256 = "10c29bd3s3ay08mvm3j6zjdiqakn1fv5m10mbqakz8ff8mqk9f32";
    };
    dependencies = [ ];
  };
}
