with import <nixpkgs/lib>;

''
  " FLAGS -----------------------------------------------------------------------
  ${builtins.readFile ./configs/flags.vim}

  " DIGRAPHS --------------------------------------------------------------------
  ${builtins.readFile ./configs/digraphs.vim}

  " GENERAL ---------------------------------------------------------------------
  ${builtins.readFile ./configs/general.vim}

  " TELESCOPE -------------------------------------------------------------------
  lua << EOF
  ${builtins.readFile ./configs/telescope.lua}
  EOF

  " KEYS ------------------------------------------------------------------------
  ${builtins.readFile ./configs/keys.vim}

  " COMMENTS --------------------------------------------------------------------
  lua << EOF
  ${builtins.readFile ./configs/comment.lua}
  EOF

  " LANGUAGE SUPPORT ------------------------------------------------------------
  ${builtins.readFile ./configs/language-support.vim}

  lua << EOF
  ${builtins.readFile ./configs/language-server.lua}
  EOF

  " DISTRACTION FREE ------------------------------------------------------------
  ${builtins.readFile ./configs/distraction-free.vim}

  " THEME -----------------------------------------------------------------------
  ${builtins.readFile ./configs/theme.vim}

  " STATUS LINE -----------------------------------------------------------------
  ${builtins.readFile ./configs/status-line.vim}
''
