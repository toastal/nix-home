{ config, lib, pkgs ? import <nixpkgs>, ...  }:

with lib;

{
  programs.kitty = {
    enable = true;

    # font = { name = "Terminus 24" }
    font = {
      package = pkgs.iosevka-fixed;
      name = "Iosevka Fixed";
    };

    settings = {
      font_size = "12.0";
      scrollback_lines = 20000;
      tab_bar_style = "separator";
      foreground = "#c0cccd";
      background = "#121116";
      cursor = "#5d5fa6";
      touch_scroll_multiplier = "5.0";
      # Base16 Atelier Cave
      # Author: Bram de Haan (http://atelierbramdehaan.nl)
      # 16 color space
      # Black, Gray, Silver, White
      color0 = "#19171c";
      color8 = "#655f6d";
      color7 = "#8b8792";
      color15 = "#f0edf5";
      # Red
      color1 = "#be4678";
      color9 = "#be4678";
      # Green
      color2 = "#2c9a9a";
      color10 = "#2c9a9a";
      # Yellow
      color3 = "#a06e3b";
      color11 = "#a06e3b";
      # Blue
      color4 = "#576ddb";
      color12 = "#576ddb";
      # Purple
      color5 = "#a06cea";
      color13 = "#a06cea";
      # Teal
      color6 = "#398bc6";
      color14 = "#398bc6";
      # Extra color
      color16 = "#aa573c";
      color17 = "#bf40bf";
      color18 = "#26232a";
      color19 = "#585260";
      color20 = "#7e7887";
      color21 = "#e2dfe7";
    };
  };
}
