{ config, lib, pkgs ? import <nixpkgs>, ... }:

with lib;

{
  programs.termite = {
    enable = true;

    allowBold = true;
    audibleBell = false;
    clickableUrl = true;
    dynamicTitle = true;
    mouseAutohide = true;
    searchWrap = true;
    scrollbackLines = 10000;

    font = "Iosevka Fixed 12";

    cursorShape = "block";
  };
}
