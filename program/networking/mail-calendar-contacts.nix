{ config, lib, pkgs ? import <nixpkgs>, ... }:

with lib;

let
  username = "toastal";
  posteoEmail = "${username}@posteo.net";
  platonicEmail = "kai.dao@platonic.systems";

  tomlFormat = pkgs.formats.toml { };

  downloadsDir = "${config.home.homeDirectory}/Downloads";
  mailDir = "${config.xdg.dataHome}/mail";
  contactsDir = "${config.xdg.dataHome}/contacts";
  calendarDir = "${config.xdg.dataHome}/calendars";

in
{
  #home.pkgs = with pkgs; [
  #  himalaya
  #  isync
  #  khal
  #];

  xdg.configFile."himalaya/config.toml".source = tomlFormat.generate "himalaya-config" {
    name = "${username}";
    downloads-dir = "${downloadsDir}";
    watch-cmds = [ "${pkgs.isync}/bin/mbsync -a" ];
    notify-cmd = "${pkgs.libnotify}/bin/notify-send";
    default-page-size = 50;
    signature = ''
      toastal ไข่ดาว | https://toast.al
      PGP: 7944 74b7 d236 dab9 c9ef  e7f9 5cce 6f14 66d4 7c9e
    '';

    posteo = {
      default = true;
      downloads-dir = "${downloadsDir}/mail/posteo/${username}";
      email = "${posteoEmail}";
      imap-host = "posteo.de";
      imap-port = 993;
      imap-login = "${posteoEmail}";
      imap-passwd-cmd = "pass show personal/posteo";
      smtp-host = "posteo.de";
      smtp-port = 465;
      smtp-login = "${posteoEmail}";
      smtp-passwd-cmd = "pass show personal/posteo";
      signature = ''
        toastal ไข่ดาว | https://toast.al
        PGP: 7944 74b7 d236 dab9 c9ef  e7f9 5cce 6f14 66d4 7c9e
      '';
    };

    platonic = {
      default = false;
      downloads-dir = "${downloadsDir}/mail/platonic/kai.dao";
      email = "${platonicEmail}";
      imap-host = "imap.gmail.com";
      imap-port = 993;
      imap-login = "${platonicEmail}";
      imap-passwd-cmd = "pass show platonic/google-app";
      smtp-host = "smtp.gmail.com";
      smtp-port = 465;
      smtp-login = "${platonicEmail}";
      smtp-passwd-cmd = "pass show platonic/google-app";
      signature = ''
        Kai Dao ไข่ดาว | Platonic.Systems | https://platonic.systems
      '';
    };
  };

  xdg.configFile."khal/config".text = ''
    [calendars]
      [[personal]]
        path = ${calendarDir}/${posteoEmail}/default
        color = light green
      [[platonic]]
        path = ${calendarDir}/${platonicEmail}/events
        color = light blue

    [default]
    default_calendar = personal

    [locale]
    timeformat = %H:%M
    dateformat = %Y-%m-%d
    longdateformat = %Y-%m-%d
    datetimeformat = %Y-%m-%d %H:%M
    longdatetimeformat = %Y-%m-%d %H:%M
  '';

  xdg.configFile."vdirsyncer/config".text = ''
    [general]
    status_path = "${config.xdg.cacheHome}/vdirsyncer/status/"

    [pair personal_contacts]
    a = "personal_contacts_local"
    b = "personal_contacts_remote"
    collections = [ "from a", "from b" ]

    [storage personal_contacts_local]
    type = "filesystem"
    path = "${contactsDir}/${posteoEmail}"
    fileext = ".vcf"

    [storage personal_contacts_remote]
    type = "carddav"
    url = "https://posteo.de:8443/addressbooks/${username}/default"
    username = "${posteoEmail}"
    password.fetch = [ "command", "pass", "show", "personal/posteo" ]
    auth = "basic"

    [pair personal_calendar]
    a = "personal_calendar_local"
    b = "personal_calendar_remote"
    collections = [ "from a", "from b" ]

    [storage personal_calendar_local]
    type = "filesystem"
    path = "${calendarDir}/${posteoEmail}"
    fileext = ".ics"

    [storage personal_calendar_remote]
    type = "caldav"
    url = "https://posteo.de:8443/calendars/${username}/default"
    username = "${posteoEmail}"
    password.fetch = [ "command", "pass", "show", "personal/posteo" ]
    auth = "basic"

    [pair platonic_calendar]
    a = "platonic_calendar_local"
    b = "platonic_calendar_remote"
    collections = [ "from a", "from b" ]

    [storage platonic_calendar_local]
    type = "filesystem"
    path = "${calendarDir}/${platonicEmail}"
    fileext = ".ics"

    [storage platonic_calendar_remote]
    type = "caldav"
    url = "https://www.google.com/calendar/dav/${platonicEmail}/events"
    username = "${platonicEmail}"
    password.fetch = [ "command", "pass", "show", "platonic/google-app" ]
    auth = "basic"
  '';

  home.file.".mbsyncrc".text = ''
    IMAPAccount posteo
    Host posteo.de
    User ${posteoEmail}
    PassCmd "${pkgs.pass}/bin/pass show personal/posteo"
    SSLType STARTTLS

    IMAPStore posteo-remote
    Account posteo

    MaildirStore posteo-local
    SubFolders Verbatim
    Path ${mailDir}/${posteoEmail}/
    Inbox ${mailDir}/${posteoEmail}/Inbox

    Channel posteo
    Far :posteo-remote:
    Near :posteo-local:
    Create Both
    Expunge Both
    Patterns *
    SyncState *

    IMAPAccount platonic
    Host imap.gmail.com
    User ${platonicEmail}
    PassCmd "${pkgs.pass}/bin/pass show platonic/google-app"
    SSLType IMAPS

    IMAPStore platonic-remote
    Account platonic

    MaildirStore platonic-local
    SubFolders Verbatim
    Path ${mailDir}/${platonicEmail}/
    Inbox ${mailDir}/${platonicEmail}/Inbox

    Channel platonic
    Far :platonic-remote:
    Near :platonic-local:
    Create Both
    Expunge Both
    Patterns *
    SyncState *
  '';
}
