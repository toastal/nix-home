{ config, lib, pkgs ? import <nixpkgs>, ... }:

with lib;

{
  # home-manager version too old
  #programs.senpai = {
  #  enable = true;
  #};

  xdg.configFile."senpai/senpai.scfg".text = ''
    address chat.sr.ht
    nickname toastal
    password-cmd pass show personal/chat.sr.ht
    mouse true
  '';
}
