{ config, lib, pkgs ? import <nixpkgs> { }, ... }:

with lib;

{
  programs.neomutt = {
    enable = true;

    vimKeys = true;
  };
}
