{ pkgs ? import <nixpkgs> { }
, blink-based-browser-bin ? "${pkgs.chromium}/bin/chromium"
, extension-id ? "ophjlpahpchlmihnnnihgmmeilfjmjjc"
}:

# TODO: icon? I personally wouldn’t see it 🤷
pkgs.makeDesktopItem {
  name = "line-messenger-app";
  desktopName = "LINE Messenger App";
  comment = "Executes LINE extension in a Blink-based browser";
  exec = "${blink-based-browser-bin} --app=\"chrome-extension://${extension-id}/index.html\"";
  terminal = "false";
  type = "Application";
  categories = "Chat";
}
