{ config, lib, pkgs ? import <nixpkgs>, ... }:

with lib;

{
  xdg.configFile."matterhorn/config.ini".text = ''
    [mattermost]
    user: kai.dao
    host: chat.platonic.systems
    passcmd: pass show platonic/chat
  '';
}
