{ config, lib, pkgs ? import <nixpkgs>, ... }:

with lib;

{
  programs.password-store = {
    enable = true;

    package = pkgs.pass.withExtensions (exts: [
      exts.pass-otp
    ]);

    settings = {
      PASSWORD_STORE_DIR = "${config.xdg.dataHome}/password-store";
      PASSWORD_STORE_KEY = "5CCE6F1466D47C9E";
    };
  };

  xdg.configFile."password-store/.extensions/first-line.besh".text = ''
    pass show "$@" | head -n 1
  '';
}
