{ config, lib, pkgs ? import <nixpkgs>, ... }:

with lib;

{
  programs.fish = {
    enable = true;
    functions = import ./functions.nix;
    plugins = import ./plugins.nix { inherit (pkgs) fetchFromGitHub; };
    interactiveShellInit = "${builtins.readFile ./interactiveShellInit.fish}";
    shellInit = "${builtins.readFile ./shellInit.fish}";
  };
}
