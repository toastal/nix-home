if test "$TERM" = 'xterm-kitty'
  kitty + complete setup fish | source
end

if test -f "/opt/asdf-vm/asdf.fish"
  source "/opt/asdf-vm/asdf.fish"
end

if type -q "any-nix-shell"
  any-nix-shell fish --info-right | source
end

if type -q "keychain"
  keychain --quiet --eval --noask --agents ssh,gpg id_rsa | source
end

set fish_vi_key_mode true
