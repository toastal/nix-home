{
  fish_prompt = {
    body = ''
      printf '%s%s%s%s%s%s ' \
        (set last_status $status) \
        (set_color $fish_color_cwd) \
        (prompt_pwd) \
        (set_color normal) \
        (__fish_git_prompt)
      set_color normal
    '';
  };

  fish_greeting = {
    body = ''
      set_color grey
      printf '%s@%s | %s %s | %s %s%% | %s\n' \
        (whoami) (hostname) (uname -s) (uname -r) \
        (cat /sys/class/power_supply/BAT0/status) \
        (cat /sys/class/power_supply/BAT0/capacity) \
        (date +'%T')
      set_color normal
    '';
  };

  __node_binpath_cwd = {
    argumentNames = [ "PWD" ];
    body = ''
      set -l node_modules_path "$PWD/node_modules/.bin"
      if test -d "$node_modules_path"
        set -g __node_binpath "$node_modules_path"
        set -x PATH $PATH $__node_binpath
      else
        set -q __node_binpath
          and set -l index (contains -i -- $__node_binpath $PATH)
          and set -e PATH[$index]
          and set -e __node_binpath
      end
    '';
  };

  subresource_integrity_from_uri = {
    argumentNames = [ "uri" ];
    body = ''
      set -l hash (curl -1 --http2 --compressed $uri | openssl dgst -sha384 -binary | openssl enc -base64 -A)
      echo "Hash: $hash"
      echo "<script src=\"$argv\" integrity=\"sha384-$hash\" crossorigin=\"anonymous\"></script>"
    '';
  };

  latest_nixpkgs_hash = {
    body = "curl --silent 'https://api.github.com/repos/NixOS/nixpkgs/branches/master' | jq -r '.commit.sha'";
  };
}
