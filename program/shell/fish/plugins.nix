{ fetchFromGitHub }:

[
    {
      name = "fish_logo";
      src = fetchFromGitHub {
        owner = "laughedelic";
        repo = "fish_logo";
        rev = "508826dd4c7b597bade75e1955b447930111e8e6";
        sha256 = "03r6ymkvqn800f5fj8vzd868392gpsasjz7jljmqyczz3j172i18";
      };
    }
    {
      name = "nix-env";
      src = fetchFromGitHub {
        owner = "lilyball";
        repo = "nix-env.fish";
        rev = "c239a69122c88797b34e3721659b2ba5060ca7e7";
        sha256 = "0hvj3zqrx5vhbhcszrgd9cczkn97236zfbx7iwjx3grnk556r53c";
      };
    }
]
