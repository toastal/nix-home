{ config, lib, pkgs ? import <nixpkgs>, ...  }:

with lib;

{
  programs.ncmpcpp = {
    enable = true;
    package = pkgs.ncmpcpp.override { visualizerSupport = true; };

    mpdMusicDir = "/music";

    settings = {
    };
  };
}
