{ config, lib, pkgs ? import <nixpkgs>, ...  }:

with lib;

{
  programs.mpv = {
    enable = true;

    config = {
      hwdec = "auto-safe";
      vo = "gpu";
      profile = "gpu-hq";
      ytdl-format = "bestvideo+bestaudio";
      cache-default = 4000000;
    };
  };
}
