# nix home

Based on: https://github.com/HugoReeves/nix-home

Use by creating a `home.nix`

```nix
{
  programs.home-manager.enable = true;
  nixpkgs.config.allowUnfree = true;

  imports = [
    ./user/toastal.nix
  ];
}
```
