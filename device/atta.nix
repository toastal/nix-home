{ config, lib, pkgs, ... }:

with lib;

{
  imports = [
    ../program/file-manager/broot/default.nix
    ../program/terminal/kitty/default.nix
  ];

  home.packages = with pkgs; [
    ncmpcpp
    screenfetch
    # jitsi
    # matcha-gtk-theme
    # gtk-engine-murrine
  ];

  home.language.base = "en-US.UTF-8";

  home.sessionVariables = {
    GDK_DPI_SCALE = "1.25";
    RUNTIME_PM_DRIVER_BLACKLIST = "nouveau";
    LOCALES_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
  };
}
