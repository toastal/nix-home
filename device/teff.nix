{ config, lib, pkgs ? import <nixpkgs>, ... }:

with lib;

let
  line-messaging-app = import ../program/networking/instant-messenger/line/default.nix {
    inherit (pkgs);
    blink-based-browser-bin = "${pkgs.brave}/bin/brave";
  };

  steam = pkgs.steam.override {
    extraPkgs = pkgs: with pkgs; [
      ffmpeg
      libidn2
      libnghttp2
      libpsl
      openssl
      pipewire
      rtmpdump
    ];
    extraLibraries = pkgs: [ pkgs.pipewire ];
  };

in
{
  xsession.pointerCursor = {
    package = pkgs.vanilla-dmz;
    name = "Vanilla-DMZ-AA";
    size = 64;
    defaultCursor = "left_ptr";
  };

  imports = [
    #../program/browser/firefox/default.nix
    ../program/video/mpv/default.nix
    ../program/file-manager/broot/default.nix
    ../program/terminal/kitty/default.nix
    ../service/mpd/default.nix
    ../service/window-manager/sway/default.nix
    ../service/window-manager/xmonad/default.nix
    ../theme/tnngle.nix
  ];


  home.packages = with pkgs; [
    # shell / terminal
    dash

    # window-manager
    grim
    mako
    kanshi
    slurp
    swaybg
    swaylock
    wl-clipboard
    wofi

    # theming
    gsettings-desktop-schemas
    gtk-engine-murrine
    gtk_engines
    vanilla-dmz

    # text editors
    leafpad

    # file managers
    xfce.thunar

    # browsers
    brave
    epiphany
    latest.firefox-beta-bin
    #latest.firefox-esr-bin
    #firefox-wayland
    #firefox-esr-wayland
    netsurf.browser

    # messaging
    line-messaging-app
    signal-desktop

    # audio / video
    ffmpeg
    ncmpcpp
    nicotine-plus
    mpdas
    pavucontrol
    #wylt

    # graphics
    darktable
    #displaycal
    hugin
    gimp
    inkscape
    krita

    # fonts
    iosevka-fixed
    julia-mono
    kanit-font
    liberation_ttf
    mplus-outline-fonts
    noto-fonts
    noto-fonts-extra
    noto-fonts-cjk
    roboto
    tlwg
    #twitter-color-emoji

    # dictionary
    #aspell
    #apsellDicts.en
    #aspellDicts.lo
    #apsellDicts.th
    hunspell
    hunspellDicts.en-us-large
    hunspellDicts.th-th

    # global programming languages
    nodejs-slim

    # android
    apktool
    androidenv.androidPkgs_9_0.platform-tools

    # keys
    gpgme
    keybase

    # other
    acpid
    dwarf-fortress
    libreoffice-fresh
    onboard
    steam
    #(steam.override { nativeOnly = true; })
    steam-run-native
    tmate
    transmission_gtk
    unclutter
    unzip
  ];

  xdg.enable = true;

  fonts = {
    fontconfig = {
      enable = lib.mkForce true;
    };
  };

  home.sessionVariables = {
    GDK_SCALE = "1";
    GDK_DPI_SCALE = "1";
    CHROME_PATH = "${pkgs.brave}/bin/brave";
    KITTY_ENABLE_WAYLAND = "1";
    LOCALES_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
    MOZ_ENABLE_WAYLAND = "1";
    MOZ_DISABLE_RDD_SANDBOX = "1";
    QT_SCREEN_SCALE_FACTORS = "1.5";
    XDG_CURRENT_DESKTOP = "sway";
    XDG_SESSION_TYPE = "wayland";
    # Because Elm didn’t build this in
    # ELM_HOME = "~/.cache/elm";
    NIXPKGS = "/nixpkgs";
    BAT_THEME = "base16";
    DICTPATH = "/run/current-system/sw/share/hunspell:/run/current-system/sw/share/hyphen";
  };

  # additional settings/overrides
  programs.mpv.config.gpu-context = "wayland";
  programs.termite.font = mkForce "monospace 12";
  # TODO: needs to be checked into the store
  #wayland.windowManager.sway.config.output."*".bg = "/nix-home/wallpaper/aertime-tnngled.jpg fill";
  #wayland.windowManager.sway.config.output."*".bg = "/nix-home/wallpaper/aertime-FANTALAND.jpg fill";
  wayland.windowManager.sway.config.output."*".bg = "/nix-home/wallpaper/aertime-betafshhhhhh.jpg fill";
  wayland.windowManager.sway.config.output."eDP-1".scale = "2.0";

  # cursor size
  # https://github.com/nix-community/home-manager/pull/1663
  #wayland.windowManager.sway.config.seat."*".xcursor_theme = "Vanilla-DMZ-AA 64";
  wayland.windowManager.sway.extraConfig = ''
    seat seat0 xcursor_theme Vanilla-DMZ-AA 64
  '';

  #xdg = {
  #  portal = {
  #    enable = true;
  #    extraPortals = with pkgs; [
  #      xdg-desktop-portal-wlr
  #      xdg-desktop-portal-gtk
  #    ];
  #    gtkUsePortal = true;
  #  };
  #};

  #home.file.".mozilla/native-messaging-hosts/gpgmejson.json" = builtins.toJSON {
  #  name = "gpgmejson";
  #  description = "JavaScript binding for GnuPG";
  #  path = "${pkgs.gpgme-json}/bin/gpgme-json";
  #  type = "stdio";
  #  allowed_extensions = [ "jid1-AQqSMBYb0a8ADg@jetpack" ];
  #};
}
