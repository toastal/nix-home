{ config, lib, pkgs, ... }:

with lib;

{
  imports = [
    ../program/file-manager/broot/default.nix
  ];

  home.packages = with pkgs; [
    dash

    hunspell
    hunspellDicts.en-us

    tmate
  ];
}

