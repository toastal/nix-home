self: super:

{
  iosevka-fixed = super.iosevka.override {
    set = "fixed";
    privateBuildPlan = {
      family = "Iosevka Fixed";
      spacing = "fixed";
      serifs = "sans";
      design = [
        "fixed"
        # "v-a-doublestorey-tailed"
        "v-seven-serifed"
        # "v-percent-rings-connected"
      ];
    };
  };
}
