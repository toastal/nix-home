{ config, lib, pkgs ? import <nixpkgs>, ... }:

with lib;

let
  black = "#000000";
  maroon = "#d60041";
  green = "#1fbd76";
  olive = "#f77f1d";
  navy = "#0787a8";
  purple = "#8655e7"; # "#6539bc";
  teal = "#00a48f";
  silver = "#c8caba";
  grey = "#515468";
  red = "#e94e39";
  lime = "#4dc987";
  yellow = "#dfad06";
  blue = "#43a6d0";
  fuschia = "#d74097";
  aqua = "#3cd1cf";
  white = "#ffffff";

  # cursorCfg = config.xsession.pointerCursor;

in
{
  home.packages = with pkgs; [
    matcha-gtk-theme
    vanilla-dmz
  ];

  gtk = {
    enable = true;

    theme = {
      name = "Matcha-dark-sea";
      package = pkgs.matcha-gtk-theme;
    };

    iconTheme = {
      name = "ePapirus";
      package = pkgs.papirus-icon-theme;
    };

    gtk3.extraConfig = {
      gtk-application-prefer-dark-theme = true;
      # gtk-cursor-theme-name = cursorCfg.name;
      # gtk-cursor-theme-size = cursorCfg.size;
    };
  };

  #programs.termite = {
  #  colorsExtra = ''
  #    color0 = ${black}
  #    color1 = ${maroon}
  #    color2 = ${green}
  #    color3 = ${olive}
  #    color4 = ${navy}
  #    color5 = ${purple}
  #    color6 = ${teal}
  #    color7 = ${silver}
  #    color8 = ${grey}
  #    color9 = ${red}
  #    color10 = ${lime}
  #    color11 = ${yellow}
  #    color12 = ${blue}
  #    color13 = ${fuschia}
  #    color14 = ${aqua}
  #    color15 = ${white}
  #  '';
  #};

  programs.kitty.settings.foreground = mkForce silver;
  programs.kitty.settings.background = mkForce black;
  programs.kitty.settings.cursor = mkForce purple;
  programs.kitty.settings.color0 = mkForce black;
  programs.kitty.settings.color1 = mkForce maroon;
  programs.kitty.settings.color2 = mkForce green;
  programs.kitty.settings.color3 = mkForce olive;
  programs.kitty.settings.color4 = mkForce navy;
  programs.kitty.settings.color5 = mkForce purple;
  programs.kitty.settings.color6 = mkForce teal;
  programs.kitty.settings.color7 = mkForce silver;
  programs.kitty.settings.color8 = mkForce grey;
  programs.kitty.settings.color9 = mkForce red;
  programs.kitty.settings.color10 = mkForce lime;
  programs.kitty.settings.color11 = mkForce yellow;
  programs.kitty.settings.color12 = mkForce blue;
  programs.kitty.settings.color13 = mkForce fuschia;
  programs.kitty.settings.color14 = mkForce aqua;
  programs.kitty.settings.color15 = mkForce white;
  programs.kitty.settings.selection_background = mkForce "#cc003a";
  programs.kitty.settings.selection_foreground = mkForce black;

  wayland.windowManager.sway.config.colors.background = black;
  wayland.windowManager.sway.config.colors.focused = {
    border = lime;
    background = grey;
    text = white;
    indicator = olive;
    childBorder = green;
  };
  wayland.windowManager.sway.config.colors.focusedInactive = {
    border = fuschia;
    background = grey;
    text = silver;
    indicator = green;
    childBorder = purple;
  };
  wayland.windowManager.sway.config.colors.unfocused = {
    border = silver;
    background = grey;
    text = silver;
    indicator = teal;
    childBorder = grey;
  };
  wayland.windowManager.sway.config.colors.urgent = {
    border = maroon;
    background = red;
    text = white;
    indicator = red;
    childBorder = maroon;
  };
}
