{ pkgs ? import <nixpkgs>, ... }:

{
  privateBuildPlan = {
    family = "Iosevka Fixed";
    design = [
      "fixed"
      "v-a-doublestorey-tailed"
      "v-seven-serifed"
      "v-percent-rings-connected"
    ];
  };
  set = "fixed";
}
